package com.cucumber;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/Resources/Feature"},
        glue = {"com.cucumber"},
        //   dryRun = true,
        tags = {"@test"}

)

public class runner {
}
