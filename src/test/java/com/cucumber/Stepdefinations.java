package com.cucumber;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Stepdefinations  {
    WebDriver driver;
    Utility util = new Utility();
    Scanner sc = new Scanner(System.in);

    @Given("^User launches the browser and navigate to \"(.*?)\" Login Page$")
    public void user_launches_the_browser_and_navigate_to_Login_Page(String URL) throws Throwable {
        String browserType = util.getproperty("browserType");

        //Static Chrome Driver Import
//        System.setProperty("webdriver.chrome.driver", " D:\\Selenium\\Assignment\\BDDPOC\\BDDPOC\\src\\test\\Resources\\Drivers\\chromedriver.exe");

        //Chrome Driver and Edge Driver
        if(browserType.contains("Chrome")) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            driver = new ChromeDriver(chromeOptions);
        } else if (browserType.contains("Edge")){
            WebDriverManager.edgedriver().setup();
            EdgeOptions edgeOptions = new EdgeOptions();
            driver = new EdgeDriver(edgeOptions);
        } else {
            System.out.println("Error");
        }

        driver.get(util.getproperty(URL));
        driver.manage().window().maximize();
        Thread.sleep(5000);
    }

    @And("^User enters the valid user id \"([^\"]*)\"$")
    public void userEntersTheValidUserId(String userID) throws Throwable {
        driver.findElement(By.xpath("(//input[@name='flyerid'])[2]")).sendKeys(util.getproperty(userID));
        Thread.sleep(3000);
    }

    @And("^User enters the valid password \"([^\"]*)\"$")
    public void userEntersTheValidPassword(String password) throws Throwable {
        driver.findElement(By.xpath("(//input[@name='pin'])[2]")).sendKeys(util.getproperty(password));
        Thread.sleep(3000);
    }

    @Then("^Click on Login Button$")
    public void clickOnLoginButton() throws InterruptedException {
        driver.findElement(By.xpath("(//button[text()='Log In'])[3]")).click();
        Thread.sleep(3000);
    }

    @Then("^Click on Plan Travel$")
    public void click_on_Plan_Travel() throws Throwable {
        driver.findElement(By.xpath("//span[text()='Plan Travel']")).click();
        Thread.sleep(3000);
    }

    @Then("^Click on \"(.*?)\" under book menu$")
    public void click_on_under_book_menu(String linktext) throws Throwable {
        if (linktext.contains("Flights")) {
            driver.findElement(By.linkText(linktext)).click();
            Thread.sleep(5000);
        } else {
            throw new NoSuchElementException("Oops! Cannot find the string");
        }
    }

    @Then("^select Trip type$")
    public void select_Trip_type() throws Throwable {
        driver.findElement(By.xpath("//*[@id=\"onewaytrip\"]")).click();
        Thread.sleep(5000);
    }

    @Then("^Enter trip details and search for flight$")
    public void enter_trip_details_and_search_for_flight() throws Throwable {
        WebDriverWait wait = new WebDriverWait(driver,30);
        String toDate = "//td[@data-handler='selectDay'][2]";
        String calendarIcon = "//input[@id='departCalendar']";

        //Source
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@name='flightSearchFrom']"))
                .sendKeys(util.getproperty("source"));
        driver.findElement(By.xpath("//div//p//span[text()='" + util.getproperty("source") + "']")).click();

        //Destination
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@name='flightSearchTo']"))
                .sendKeys(util.getproperty("destination"));
        driver.findElement(By.xpath("//div//p//span[text()='" + util.getproperty("destination") + "']")).click();

        //Journey Date Picker - selects the next calendar date
        driver.findElement(By.xpath(calendarIcon)).click();
        driver.findElement(By.xpath(toDate)).click();

        Thread.sleep(5000);
        driver.findElement(By.xpath("//button[text()='Search Flights']")).click();
        Thread.sleep(50000);
    }

    @And("Read all the filght list and select the feasible one")
    public void readAllTheFilghtListAndSelectTheFeasibleOne() throws InterruptedException {
        List<String> currentOptions = new ArrayList<>();

        List<WebElement> prices = driver.findElements(By.xpath("//span[@class='cell-reco-bestprice-integer availability-font-xlarge']"));
        for (WebElement match : prices) {
            currentOptions.add(match.getText());
        }

        String current = "";
        for(int i =0;i<currentOptions.size();i++)
        {
            current = currentOptions.get(i);
        }
        System.out.println(current);
        Optional<String> minNumber;
        minNumber = currentOptions.stream().min(new Comparator<String>() {
            @Override
            public int compare(String i, String j) {
                return i.compareTo(j);
            }
        });
        String finalPrice =  minNumber.get();
        System.out.println(finalPrice);
        Thread.sleep(3000);
//        System.out.println("Booking Complete");
        driver.quit();
    }
}

