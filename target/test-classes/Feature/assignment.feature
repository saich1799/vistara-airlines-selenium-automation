Feature: Flight Booking

  @test
  Scenario:Searching availbility of a flight as a resgistered user
    Given User launches the browser and navigate to "VistaraAirlines" Login Page
    And User enters the valid user id "userName"
    And User enters the valid password "passcode"
    Then Click on Login Button
    Then Click on Plan Travel
    Then Click on "Flights" under book menu
    Then select Trip type
    Then Enter trip details and search for flight
    And Read all the filght list and select the feasible one
